//
//  main.m
//  Archiver2
//
//  Created by Daniel Ross on 7/28/11.
//  Copyright Demco Printing 2011. All rights reserved.
//

#import <MacRuby/MacRuby.h>

int main(int argc, char *argv[])
{
    return macruby_main("rb_main.rb", argc, argv);
}
