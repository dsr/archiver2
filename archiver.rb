require 'find'

def search(term, file, type = nil)
	
	
	counter = 1
	if type == "word"
		### search only whole words
		results = IO.popen("grep -iw #{term} '#{file}'")
	else
		results = IO.popen("grep -i #{term} '#{file}'")
	end
	# puts results
	return results.readlines
	
end

# test = search("cri", "/Volumes/MACVOLUME/_Job Archive Files_/Jobs 2010.txt")
# test.each do |line|
# 	puts line
# end

def create_archive(sender, file)
	puts "create archive clicked"
	puts file
	File.open(file, 'w') 
end

def add_to_archive(sender, archive_contents, archive_file)
	Find.find(archive_contents) do |f|
		type = "Directory" if File.directory?(f)
		type = "File" if File.file?(f)
		File.open(archive_file, 'a') do |file|
			file.write("#{type}: #{f} \n")
		end
	end
end
