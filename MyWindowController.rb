# MyWindowController.rb
# Archiver2
#
# Created by Daniel Ross on 7/28/11.
# Copyright 2011 Demco Printing. All rights reserved.
require 'archiver'

class MyWindowController < NSWindowController
	attr_accessor :search_text, :results_box, :destination_path, :progress_bar
	def manip(x)
		return x.reverse
	end
  
	def search_button_clicked(sender)
  # Create the File Open Dialog class.
    dialog = NSOpenPanel.openPanel
    # Disable the selection of files in the dialog.
    dialog.canChooseFiles = true
    # Enable the selection of directories in the dialog.
    dialog.canChooseDirectories = false
    # Disable the selection of multiple items in the dialog.
    dialog.allowsMultipleSelection = false
    
    # Display the dialog and process the selected folder
    if dialog.runModal() == NSOKButton
      @results_box.setString("")
      @file = dialog.filename
      @progress_bar.startAnimation(nil)
      # puts "File #{@file}"
      # puts "Search for #{@search_text.stringValue}"
      @searching = Thread.new {
        search(@search_text.stringValue, @file)
        # @results = search(@search_text.stringValue, @file)
        # @results.each do |result|
        #   @results_box.insertText(result + "\n") unless result.include?('gr_')
        # end

      }
      # puts @searching.value
      @searching.value.each do |result|  
        @results_box.insertText(result + "\n") unless result.include?('gr_')
      end
      
      @progress_bar.stopAnimation(nil)
    else
      return
    end
	end
	
  def whole_word_search_button_clicked(sender)
  # Create the File Open Dialog class.
    dialog = NSOpenPanel.openPanel
    # Disable the selection of files in the dialog.
    dialog.canChooseFiles = true
    # Enable the selection of directories in the dialog.
    dialog.canChooseDirectories = false
    # Disable the selection of multiple items in the dialog.
    dialog.allowsMultipleSelection = false
    
    # Display the dialog and process the selected folder
    if dialog.runModal() == NSOKButton
      @results_box.setString("")
      @file = dialog.filename
      @progress_bar.startAnimation(nil)
      # puts "File #{@file}"
      # puts "Search for #{@search_text.stringValue}"
      @searching = Thread.new {
        search(@search_text.stringValue, @file, 'word')
      }
      # puts @searching.value
      @searching.value.each do |result|
        @results_box.insertText(result + "\n") unless result.include?('gr_')
      end
      
      @progress_bar.stopAnimation(nil)
    else
      return
    end
  end
  
  def create_button_clicked(sender)
    dialog = NSSavePanel.savePanel()
    dialog.title = "Create a new archive file"
    if dialog.runModal == NSOKButton
      @file = dialog.filename()
      create_archive(sender, @file)
    end
	end

  def add_button_clicked(sender)
    dialog1 = NSOpenPanel.openPanel
    dialog1.title = "Choose an archive to append"
    dialog1.canChooseFiles = true
    dialog1.canChooseDirectories = false
    dialog1.allowsMultipleSelection = false
    if dialog1.runModal == NSOKButton
      @archive_file = dialog1.filename
    end
    dialog2 = NSOpenPanel.openPanel
    dialog2.title = "Choose the disc to archive"
    dialog2.canChooseFiles = true
    dialog2.canChooseDirectories = true 
    dialog2.allowsMultipleSelection = false
    if dialog2.runModal() == NSOKButton
      @archive_contents = dialog2.filename
      add_to_archive(sender, @archive_contents, @archive_file)
    end
  end
end